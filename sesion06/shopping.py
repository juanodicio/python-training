from collections import namedtuple

class ExcesoDeProductosError(Exception): pass
class CarritoVacioError(Exception): pass
class CreditoInsuficienteError(Exception): pass

valor = 30
LineaOrden = namedtuple("LineaOrden", ["producto", "precio"])

class CarritoComprasDominio:
    def __init__(self):
        self.lineas : list[LineaOrden] = []

    def __len__(self):
        return len(self.lineas)

    def agregar(self, linea: LineaOrden):
        if len(self) >= 2:
            raise ExcesoDeProductosError("No puede comprar más de 2 productos")
        self.lineas.append(linea)

    def registrar_orden(self):
        if len(self) == 0:
            raise CarritoVacioError("No se pueden registrar órdenes sin productos.")
        
        if sum([linea.precio for linea in self.lineas]) > 100:
            raise CreditoInsuficienteError("Monto total excede su crédito")

        print("Orden registrada.")
    
    def reiniciar(self):
        self.lineas.clear()




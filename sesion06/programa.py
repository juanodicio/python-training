from shopping import CarritoComprasDominio, LineaOrden, CarritoVacioError, ExcesoDeProductosError
import shopping

carrito = CarritoComprasDominio()
carrito.agregar(LineaOrden("Pizza", 0)) 
carrito.agregar(LineaOrden("Hamburguesa", 230))

try:
    carrito.agregar(LineaOrden("Hamburguesa", 14))
    carrito.registrar_orden()
except CarritoVacioError:
    print("El carrito de compras está vacío")
except ExcesoDeProductosError:
    print("Tiene demasiados productos en el carrito")
else:
    print("Orden registrada :-)")


print(__name__)
print(shopping.__name__)
print(shopping.__file__)
print(f"Valor desde programa: {shopping.valor}")

shopping.valor = 40


from log import log

log.info("dddd")
import unittest
from unittest import mock
from calculadora import Calculadora


class TestCalc(unittest.TestCase):

    def test_suma(self):
        # AAA

        # Arrange
        calc = Calculadora()

        # Act
        resp = calc.sumar(10, 5)

        # Assert
        self.assertEqual(resp, 15)

    def test_multiplicacion(self):
        calc = Calculadora()

        resp = calc.multiplicar(10, 5)
        
        self.assertEqual(50, resp)

    def test_error_al_dividir_entre_cero(self):
        calc = Calculadora()

        with self.assertRaises(ZeroDivisionError):
            calc.dividir(50, 0)


from decimal import DivisionByZero
from .calculadora import Calculadora
import pytest


def test_sumar():
    calc = Calculadora()

    resp = calc.sumar(150, 50)

    assert resp == 200


def test_error_al_dividir_entre_cero():
    calc = Calculadora()

    with pytest.raises(ZeroDivisionError):
        calc.dividir(5, 0)

    